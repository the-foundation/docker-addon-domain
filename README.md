# Docker domain proxy

This is a pure forwarding docker container , e.g. for adding domains with single certificates ( that might fail without breaking all other domains  in letsencrypt cert) or when using websockets to have wss://point to a different url

## Installation :
* create .env with `APP_URL` `TARGET_URL` and optionally `TARGET_PORT` 
* when `TARGET_PORT` is undefined, it defaults to:
* * port `80`  for the http  variant
* * port `443` for the https variant 
* soft link the matching docker-compose file (`ln -s docker-compose-mysettings.yml docker-compose.yml`)
or use `-f docker-compose-mysettings.yml` )
## Variants:
* Fulldomain: generates www.APP_URL and www.APP_URL
* Singledomain: generates only APP_URL
---


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/docker-addon-domain/README.md/logo.jpg" width="480" height="270"/></div></a>
